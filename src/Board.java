import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;

/**
 * Reprents a game board.
 */
public class Board {
	private char[][] board;

	public static final char SPACE = ' ';
	public static final char BOX = '$';
	public static final char WALL = '#';
	public static final char GOAL = '.';
	public static final char BOX_ON_GOAL = '*';
	public static final char PLAYER = '@';
	public static final char PLAYER_ON_GOAL = '+';
	public static final char DEADLOCK_POS = '%';
	public static final char PLAYER_ON_DEADLOCK = '&';

	// 0 = blanksteg ( )
	// 1 = v�gg (#)
	// 2 = l�da ($)
	// 3 = m�l (.)
	// 4 = m�l med l�da (*)
	// 5 = spelare (@)
	// 6 = spelare på mål (+)

	/**
	 * Default constructor.
	 * 
	 * @param board
	 *            - 2d representation of the board.
	 */
	public Board(char[][] board) {
		this.board = board;
	}

	public Board copy(){
		return new Board(deepCopy(board));
	}

	/**
	 * Performs an UP-move on this board.
	 * 
	 * @return true if the move was legal, otherwise false
	 */
	public Board goUp() {
		char[][] newBoard = Board.deepCopy(this.board);

		Coordinate player = getPlayerPosition(this);
		final int index_player_row = player.getRow();
		final int index_player_column = player.getColumn();
		final int index_above_player = player.getRow() - 1;
		final int index_above_above_player = player.getRow() - 2;

		char above = board[index_above_player][index_player_column];

		if (newBoard[index_player_row][index_player_column] == PLAYER) {
			// This is just an optimization
			if (above == WALL)
				return null;

			if (above == SPACE) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_above_player][index_player_column] = PLAYER;
				return new Board(newBoard);
			}

			if (above == GOAL) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_above_player][index_player_column] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if(above == DEADLOCK_POS){
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_above_player][index_player_column] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char above_above = 'z';
			if (index_above_above_player >= 0)
				above_above = board[index_above_above_player][index_player_column];

			if (above == BOX && (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_above_player][index_player_column] = PLAYER;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_above_player][index_player_column] = PLAYER;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (above == BOX_ON_GOAL
					&& (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_above_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_above_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is standing on goal.
		else if(newBoard[index_player_row][index_player_column] == PLAYER_ON_GOAL){
			// This is just an optimization
			if (above == WALL)
				return null;

			if (above == SPACE) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_above_player][index_player_column] = PLAYER;
				return new Board(newBoard);
			}

			if (above == GOAL) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_above_player][index_player_column] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if(above == DEADLOCK_POS){
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_above_player][index_player_column] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char above_above = 'z';
			if (index_above_above_player >= 0)
				above_above = board[index_above_above_player][index_player_column];

			if (above == BOX && (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = PLAYER;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = PLAYER;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (above == BOX_ON_GOAL
					&& (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is standing on a deadlock.
		else if(newBoard[index_player_row][index_player_column] == PLAYER_ON_DEADLOCK){
			// This is just an optimization
			if (above == WALL)
				return null;

			if (above == SPACE) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_above_player][index_player_column] = PLAYER;
				return new Board(newBoard);
			}

			if (above == GOAL) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_above_player][index_player_column] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if(above == DEADLOCK_POS){
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_above_player][index_player_column] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char above_above = 'z';
			if (index_above_above_player >= 0)
				above_above = board[index_above_above_player][index_player_column];

			if (above == BOX && (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = PLAYER;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = PLAYER;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (above == BOX_ON_GOAL
					&& (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is not nearby, Jons space-pusher  Player == Space
		else if(newBoard[index_player_row][index_player_column] == SPACE){
			// This is just an optimization
			if (above == WALL)
				return null;

			char above_above = 'z';
			if (index_above_above_player >= 0)
				above_above = board[index_above_above_player][index_player_column];

			if (above == BOX && (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_above_player][index_player_column] = SPACE;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_above_player][index_player_column] = SPACE;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (above == BOX_ON_GOAL
					&& (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_above_player][index_player_column] = GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_above_player][index_player_column] = GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is not nearby, Jons space-pusher  Player == GOAL

		else{
			// This is just an optimization
			if (above == WALL)
				return null;

			char above_above = 'z';
			if (index_above_above_player >= 0)
				above_above = board[index_above_above_player][index_player_column];

			if (above == BOX && (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = SPACE;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = SPACE;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (above == BOX_ON_GOAL
					&& (above_above == SPACE || above_above == GOAL)) {
				if (above_above == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (above_above == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_above_player][index_player_column] = GOAL;
					newBoard[index_above_above_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		

		return null;
	}

	/**
	 * Performs a DOWN-move on this board.
	 * 
	 * @return A new board. Null if the move was illegal.
	 */
	public Board goDown() {
		char[][] newBoard = Board.deepCopy(this.board);

		StringBuilder sb = new StringBuilder();
		for (int r = 0; r < newBoard.length; r++) {
			for (int c = 0; c < newBoard[r].length; c++) {
				sb.append((newBoard[r][c]));
			}
			sb.append('\n');
		}
		Coordinate player = getPlayerPosition(this);

		final int index_player_row = player.getRow();
		final int index_player_column = player.getColumn();
		final int index_below_player = player.getRow() + 1;
		final int index_below_below_player = player.getRow() + 2;

		char below = board[index_below_player][index_player_column];

		if (newBoard[index_player_row][index_player_column] == PLAYER) {
			// This is just an optimization
			if (below == WALL)
				return null;

			if (below == SPACE) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_below_player][index_player_column] = PLAYER;

				return new Board(newBoard);
			}

			if (below == GOAL) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_below_player][index_player_column] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if (below == DEADLOCK_POS) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_below_player][index_player_column] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char below_below = 'z';
			if (index_below_below_player <= board.length)
				below_below = board[index_below_below_player][index_player_column];

			if (below == BOX && (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_below_player][index_player_column] = PLAYER;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_below_player][index_player_column] = PLAYER;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (below == BOX_ON_GOAL
					&& (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_below_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_below_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is standing on goal.
		else if (newBoard[index_player_row][index_player_column] == PLAYER_ON_GOAL){
			// This is just an optimization
			if (below == WALL)
				return null;

			if (below == SPACE) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_below_player][index_player_column] = PLAYER;
				return new Board(newBoard);
			}

			if (below == GOAL) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_below_player][index_player_column] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if (below == DEADLOCK_POS) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_below_player][index_player_column] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char below_below = 'z';
			if (index_below_below_player <= board.length)
				below_below = board[index_below_below_player][index_player_column];

			if (below == BOX && (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = PLAYER;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = PLAYER;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (below == BOX_ON_GOAL
					&& (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}

		// PLayer is standing on deadlock.
		else if(newBoard[index_player_row][index_player_column] == PLAYER_ON_DEADLOCK){
			// This is just an optimization
			if (below == WALL)
				return null;

			if (below == SPACE) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_below_player][index_player_column] = PLAYER;
				return new Board(newBoard);
			}

			if (below == GOAL) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_below_player][index_player_column] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if (below == DEADLOCK_POS) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_below_player][index_player_column] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char below_below = 'z';
			if (index_below_below_player <= board.length)
				below_below = board[index_below_below_player][index_player_column];

			if (below == BOX && (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = PLAYER;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = PLAYER;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (below == BOX_ON_GOAL
					&& (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = PLAYER_ON_GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is not nearby, Jons space-pusher  Player == Space
		else if(newBoard[index_player_row][index_player_column] == SPACE){
			// This is just an optimization
			if (below == WALL)
				return null;

			char below_below = 'z';
			if (index_below_below_player >= 0)
				below_below = board[index_below_below_player][index_player_column];

			if (below == BOX && (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_below_player][index_player_column] = SPACE;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_below_player][index_player_column] = SPACE;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (below == BOX_ON_GOAL
					&& (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_below_player][index_player_column] = GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_below_player][index_player_column] = GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is not nearby, Jons space-pusher  Player == GOAL

		else{
			// This is just an optimization
			if (below == WALL)
				return null;

			char below_below = 'z';
			if (index_below_below_player >= 0)
				below_below = board[index_below_below_player][index_player_column];

			if (below == BOX && (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = SPACE;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = SPACE;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (below == BOX_ON_GOAL
					&& (below_below == SPACE || below_below == GOAL)) {
				if (below_below == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (below_below == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_below_player][index_player_column] = GOAL;
					newBoard[index_below_below_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}

		return null;
	}

	/**
	 * Performs a LEFT-move on this board.
	 * 
	 * @return A new board. Null if the move was illegal.
	 */
	public Board goLeft() {
		char[][] newBoard = Board.deepCopy(this.board);

		StringBuilder sb = new StringBuilder();
		for (int r = 0; r < newBoard.length; r++) {
			for (int c = 0; c < newBoard[r].length; c++) {
				sb.append((newBoard[r][c]));
			}
			sb.append('\n');
		}
		Coordinate player = getPlayerPosition(this);

		final int index_player_row = player.getRow();
		final int index_player_column = player.getColumn();
		final int index_left_player = player.getColumn() - 1;
		final int index_left_left_player = player.getColumn() - 2;

		char left = board[index_player_row][index_left_player];

		if (newBoard[index_player_row][index_player_column] == PLAYER) {
			// This is just an optimization
			if (left == WALL)
				return null;

			if (left == SPACE) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_player_row][index_left_player] = PLAYER;
				return new Board(newBoard);
			}

			if (left == GOAL) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_player_row][index_left_player] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if (left == DEADLOCK_POS) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_player_row][index_left_player] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char left_left = 'z';
			if (index_left_left_player >= 0)
				left_left = board[index_player_row][index_left_left_player];

			if (left == BOX && (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_player_row][index_left_player] = PLAYER;
					newBoard[index_player_row][index_left_left_player] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_player_row][index_left_player] = PLAYER;
					newBoard[index_player_row][index_left_left_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (left == BOX_ON_GOAL
					&& (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_player_row][index_left_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_left_left_player] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_player_row][index_left_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_left_left_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is standing on goal.
		else if(newBoard[index_player_row][index_player_column] == PLAYER_ON_GOAL){
			// This is just an optimization
			if (left == WALL)
				return null;

			if (left == SPACE) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_player_row][index_left_player] = PLAYER;
				return new Board(newBoard);
			}

			if (left == GOAL) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_player_row][index_left_player] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if (left == DEADLOCK_POS) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_player_row][index_left_player] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char left_left = 'z';
			if (index_left_left_player >= 0)
				left_left = board[index_player_row][index_left_left_player];

			if (left == BOX && (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_left_player] = PLAYER;
					newBoard[index_player_row][index_left_left_player] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_left_player] = PLAYER;
					newBoard[index_player_row][index_left_left_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (left == BOX_ON_GOAL
					&& (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_left_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_left_left_player] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_left_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_left_left_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}

		// Player is standing on deadlock.
		else if(newBoard[index_player_row][index_player_column] == PLAYER_ON_DEADLOCK){
			// This is just an optimization
			if (left == WALL)
				return null;

			if (left == SPACE) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_player_row][index_left_player] = PLAYER;
				return new Board(newBoard);
			}

			if (left == GOAL) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_player_row][index_left_player] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if (left == DEADLOCK_POS) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_player_row][index_left_player] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char left_left = 'z';
			if (index_left_left_player >= 0)
				left_left = board[index_player_row][index_left_left_player];

			if (left == BOX && (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_left_player] = PLAYER;
					newBoard[index_player_row][index_left_left_player] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_left_player] = PLAYER;
					newBoard[index_player_row][index_left_left_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (left == BOX_ON_GOAL
					&& (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_left_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_left_left_player] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_left_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_left_left_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		else if(newBoard[index_player_row][index_player_column] == SPACE){
			// This is just an optimization
			if (left == WALL)
				return null;

			char left_left = 'z';
			if (index_left_left_player >= 0)
				left_left = board[index_left_left_player][index_player_column];

			if (left == BOX && (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_left_player][index_player_column] = SPACE;
					newBoard[index_left_left_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_left_player][index_player_column] = SPACE;
					newBoard[index_left_left_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (left == BOX_ON_GOAL
					&& (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_left_player][index_player_column] = GOAL;
					newBoard[index_left_left_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_left_player][index_player_column] = GOAL;
					newBoard[index_left_left_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is not nearby, Jons space-pusher  Player == GOAL

		else{
			// This is just an optimization
			if (left == WALL)
				return null;

			char left_left = 'z';
			if (index_left_left_player >= 0)
				left_left = board[index_left_left_player][index_player_column];

			if (left == BOX && (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_left_player][index_player_column] = SPACE;
					newBoard[index_left_left_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_left_player][index_player_column] = SPACE;
					newBoard[index_left_left_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (left == BOX_ON_GOAL
					&& (left_left == SPACE || left_left == GOAL)) {
				if (left_left == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_left_player][index_player_column] = GOAL;
					newBoard[index_left_left_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (left_left == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_left_player][index_player_column] = GOAL;
					newBoard[index_left_left_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}


		return null;
	}

	/**
	 * Performs a RIGHT-move on this board.
	 * 
	 * @return A new board. Null if the move was illegal.
	 */
	public Board goRight() {

		char[][] newBoard = Board.deepCopy(this.board);

		StringBuilder sb = new StringBuilder();
		for (int r = 0; r < newBoard.length; r++) {
			for (int c = 0; c < newBoard[r].length; c++) {
				sb.append((newBoard[r][c]));
			}
			sb.append('\n');
		}
		Coordinate player = getPlayerPosition(this);

		final int index_player_row = player.getRow();
		final int index_player_column = player.getColumn();
		final int index_right_player = player.getColumn() + 1;
		final int index_right_right_player = player.getColumn() + 2;

		char right = board[index_player_row][index_right_player];

		if (newBoard[index_player_row][index_player_column] == PLAYER) {
			// This is just an optimization
			if (right == WALL)
				return null;

			if (right == SPACE) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_player_row][index_right_player] = PLAYER;
				return new Board(newBoard);
			}

			if (right == GOAL) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_player_row][index_right_player] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if (right == DEADLOCK_POS) {
				newBoard[index_player_row][index_player_column] = SPACE;
				newBoard[index_player_row][index_right_player] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char right_right = 'z';
			if (index_right_right_player < board[index_player_row].length)
				right_right = board[index_player_row][index_right_right_player];

			if (right == BOX && (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_player_row][index_right_player] = PLAYER;
					newBoard[index_player_row][index_right_right_player] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_player_row][index_right_player] = PLAYER;
					newBoard[index_player_row][index_right_right_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (right == BOX_ON_GOAL
					&& (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_player_row][index_right_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_right_right_player] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_player_row][index_right_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_right_right_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is standing on goal.
		else if (newBoard[index_player_row][index_player_column] == PLAYER_ON_GOAL){
			// This is just an optimization
			if (right == WALL)
				return null;

			if (right == SPACE) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_player_row][index_right_player] = PLAYER;
				return new Board(newBoard);
			}

			if (right == GOAL) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_player_row][index_right_player] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if (right == DEADLOCK_POS) {
				newBoard[index_player_row][index_player_column] = GOAL;
				newBoard[index_player_row][index_right_player] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char right_right = 'z';
			if (index_right_right_player < board[index_player_row].length)
				right_right = board[index_player_row][index_right_right_player];

			if (right == BOX && (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_right_player] = PLAYER;
					newBoard[index_player_row][index_right_right_player] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_right_player] = PLAYER;
					newBoard[index_player_row][index_right_right_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (right == BOX_ON_GOAL
					&& (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_right_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_right_right_player] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_right_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_right_right_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}

		//Player is standing on deadlock
		else if(newBoard[index_player_row][index_player_column] == PLAYER_ON_DEADLOCK){
			// This is just an optimization
			if (right == WALL)
				return null;

			if (right == SPACE) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_player_row][index_right_player] = PLAYER;
				return new Board(newBoard);
			}

			if (right == GOAL) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_player_row][index_right_player] = PLAYER_ON_GOAL;
				return new Board(newBoard);
			}

			if (right == DEADLOCK_POS) {
				newBoard[index_player_row][index_player_column] = DEADLOCK_POS;
				newBoard[index_player_row][index_right_player] = PLAYER_ON_DEADLOCK;
				return new Board(newBoard);
			}

			char right_right = 'z';
			if (index_right_right_player < board[index_player_row].length)
				right_right = board[index_player_row][index_right_right_player];

			if (right == BOX && (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_right_player] = PLAYER;
					newBoard[index_player_row][index_right_right_player] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_right_player] = PLAYER;
					newBoard[index_player_row][index_right_right_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (right == BOX_ON_GOAL
					&& (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_right_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_right_right_player] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_player_row][index_right_player] = PLAYER_ON_GOAL;
					newBoard[index_player_row][index_right_right_player] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		else if(newBoard[index_player_row][index_player_column] == SPACE){
			// This is just an optimization
			if (right == WALL)
				return null;

			char right_right = 'z';
			if (index_right_right_player >= 0)
				right_right = board[index_right_right_player][index_player_column];

			if (right == BOX && (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_right_player][index_player_column] = SPACE;
					newBoard[index_right_right_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_right_player][index_player_column] = SPACE;
					newBoard[index_right_right_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (right == BOX_ON_GOAL
					&& (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_right_player][index_player_column] = GOAL;
					newBoard[index_right_right_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = SPACE;
					newBoard[index_right_player][index_player_column] = GOAL;
					newBoard[index_right_right_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}
		// Player is not nearby, Jons space-pusher  Player == GOAL

		else{
			// This is just an optimization
			if (right == WALL)
				return null;

			char right_right = 'z';
			if (index_right_right_player >= 0)
				right_right = board[index_right_right_player][index_player_column];

			if (right == BOX && (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_right_player][index_player_column] = SPACE;
					newBoard[index_right_right_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_right_player][index_player_column] = SPACE;
					newBoard[index_right_right_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}

			if (right == BOX_ON_GOAL
					&& (right_right == SPACE || right_right == GOAL)) {
				if (right_right == SPACE) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_right_player][index_player_column] = GOAL;
					newBoard[index_right_right_player][index_player_column] = BOX;
					return new Board(newBoard);
				}
				if (right_right == GOAL) {
					newBoard[index_player_row][index_player_column] = GOAL;
					newBoard[index_right_player][index_player_column] = GOAL;
					newBoard[index_right_right_player][index_player_column] = BOX_ON_GOAL;
					return new Board(newBoard);
				}
			}
		}


		return null;
	}

	public static char[][] rotate(char[][] m) {
		char[][] rotateM = new char[m[0].length][m.length];
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[0].length; j++) {
				rotateM[i][j] = m[j][m.length - i - 1];
			}
		}
		return rotateM;
	}

	public void markWallDeadlocks() {
		LinkedList<Coordinate> walldl;

		for(int col = 1; col < board[0].length - 1; col++) {
			for(int row = 1; row < board.length - 1; row++) {

				//Upper right corner
				if(board[row - 1][col] == WALL && board[row][col + 1] == WALL) {
					if(board[row][col] == PLAYER)
						board[row][col] = PLAYER_ON_DEADLOCK;
					else 
						board[row][col] = DEADLOCK_POS;
					walldl = new LinkedList<Coordinate>();
					for(int row2 = row + 1; row2 < board.length - 1; row2++) {
						if(board[row2][col] == SPACE && board[row2][col + 1] == WALL) {
							Coordinate dl = new Coordinate(row2, col);
							walldl.add(dl);
						}
						else {
							break;
						}
					}
					for(Coordinate d : walldl) {
						if(board[d.getColumn()][d.getRow()] != PLAYER)
							board[d.getColumn()][d.getRow()] = DEADLOCK_POS;
					}
				}
				//Lower right corner
				else if(board[row + 1][col] == WALL && board[row][col + 1] == WALL) {
					if(board[row][col] == PLAYER)
						board[row][col] = PLAYER_ON_DEADLOCK;
					else 
						board[row][col] = DEADLOCK_POS;
					walldl = new LinkedList<Coordinate>();
					for(int col2 = col - 1; col2 > 0; col2--) {
						if(board[row][col2] == SPACE && board[row + 1][col2] == WALL) {
							Coordinate dl = new Coordinate(row, col2);
							walldl.add(dl);
						}
						else {
							break;
						}
					}
					for(Coordinate d : walldl) {
						if(board[d.getColumn()][d.getRow()] != PLAYER)
							board[d.getColumn()][d.getRow()] = DEADLOCK_POS;
					}
				}
				//upper left corner
				else if(board[row - 1][col] == WALL && board[row][col - 1] == WALL) {
					if(board[row][col] == PLAYER)
						board[row][col] = PLAYER_ON_DEADLOCK;
					else 
						board[row][col] = DEADLOCK_POS;
					walldl = new LinkedList<Coordinate>();
					for(int col2 = col + 1; col2 < board[0].length; col2++) {
						if(board[row][col2] == SPACE && board[row - 1][col2] == WALL) {
							Coordinate dl = new Coordinate(row, col2);
							walldl.add(dl);
						}
						else {
							break;
						}

					}
					for(Coordinate d : walldl) {
						if(board[d.getColumn()][d.getRow()] != PLAYER)
							board[d.getColumn()][d.getRow()] = DEADLOCK_POS;
					}
				}
				//Lower left corner
				else if(board[row + 1][col] == WALL && board[row][col - 1] == WALL) {
					if(board[row][col] == PLAYER)
						board[row][col] = PLAYER_ON_DEADLOCK;
					else 
						board[row][col] = DEADLOCK_POS;
					walldl = new LinkedList<Coordinate>();
					for(int row2 = row - 1; row2 > 0; row2--) {
						if(board[row2][col] == SPACE && board[row2][col - 1] == WALL) {
							Coordinate dl = new Coordinate(row2, col);
							walldl.add(dl);
						}
						else {
							break;
						}

					}
					for(Coordinate d : walldl) {
						if(board[d.getColumn()][d.getRow()] != PLAYER)
							board[d.getColumn()][d.getRow()] = DEADLOCK_POS;
					}
				}
			}
		}
	}

	/**
	 * Ändra till koordinater input
	 */

	public boolean isDeadLock(Coordinate coord) {
		boolean PRINT_DEADLOCK = false;
		if (coord == null) {
			return false;
		}
		//		
		boolean  square =  isSquareDeadLock(coord.getColumn(), coord.getRow());
		boolean wall = isWallDeadLock(coord.getColumn(), coord.getRow());
		boolean tetris = isTetrisDeadLock(coord.getColumn(), coord.getRow());
		//		  System.out.println("IsWallDeadlock: " + wall);
		if(PRINT_DEADLOCK){
			if(square)
				System.out.println("=====SQUARE=====");
			if(wall)
				System.out.println("=====WALL=====");
			if(tetris)
				System.out.println("=====TETRIS=====");
		}
		return wall || tetris || square;		
	}

	private boolean isTetrisDeadLock(int col, int row) {

		// We must allow these boards even if they do deadlock because they might be a necessary part of the solution.

		/*
		 * 7 8 9
		 * 4 5 6
		 * 1 2 3
		 * 
		 * 5 is the incoming row/col pair
		 */
		char one = board[row + 1][col - 1];
		char two = board[row + 1][col];
		char three = board[row + 1][col + 1];
		char four = board[row][col - 1];
		char five = board[row][col];
		char six = board[row][col + 1];
		char seven = board[row - 1][col - 1];
		char eight = board[row - 1][col];
		char nine = board[row - 1][col + 1];

		boolean _9 = (nine == BOX || nine == WALL);
		boolean _8 = (eight == BOX || eight == WALL);
		boolean _7 = (seven == BOX || seven == WALL);
		boolean _6 = (six == BOX || six == WALL);
		boolean _5 = (five == BOX || five == WALL);
		boolean _4 = (four == BOX || four == WALL);
		boolean _3 = (three == BOX || three == WALL);
		boolean _2 = (two == BOX || two == WALL);
		boolean _1 = (one == BOX || one == WALL);


		//Removes some tetris deadlocks but they are covered by isSquareDeadlock so it's OK
		if (five == BOX_ON_GOAL && ( eight == BOX_ON_GOAL || six == BOX_ON_GOAL || two == BOX_ON_GOAL ||  four == BOX_ON_GOAL)) {
			return false;
		}
		else {
			if (_8) {
				if ((four == WALL  && nine == WALL)
						|| (six == WALL && seven == WALL)){
					return true;
				}
			}
			if (_2) {
				if ((four == WALL && three == WALL)
						|| (six == WALL && one == WALL)) {
					return true;
				}
			}	
			if ((four == SPACE || four == GOAL)&& _6) {
				if ((two == WALL && nine == WALL)
						|| (eight == WALL && three == WALL)){
					return true;
				}
			}

			if ((six == SPACE || six == GOAL) && _4) {
				if ((eight == WALL && one == WALL)
						|| (seven == WALL && two == WALL)) {
					return true;
				}
			}
		}
		return false;
	}


	private boolean isSquareDeadLock(int col, int row) {

		// We must allow these boards even if they do deadlock because they might be a necessary part of the solution.
		if(board[row][col] == BOX_ON_GOAL)
			return false;

		char eight = board[row - 1][col];
		char nine = board[row - 1][col + 1];
		char seven = board[row - 1][col - 1];
		char two = board[row + 1][col];
		char five = board[row][col];
		char three = board[row + 1][col + 1];
		char one = board[row + 1][col - 1];
		char six = board[row][col + 1];
		char four = board[row][col - 1];

		boolean 
		_9 = (nine == BOX || nine == BOX_ON_GOAL || nine == WALL);
		boolean _8 = (eight == BOX || eight == BOX_ON_GOAL || eight == WALL);
		boolean _7 = (seven == BOX || seven == BOX_ON_GOAL || seven == WALL);
		boolean _6 = (six == BOX || six == BOX_ON_GOAL || six == WALL);
		boolean _5 = (five == BOX || five == BOX_ON_GOAL || five == WALL);
		boolean _4 = (four == BOX || four == BOX_ON_GOAL || four == WALL);
		boolean _3 = (three == BOX || three == BOX_ON_GOAL || three == WALL);
		boolean _2 = (two == BOX || two == BOX_ON_GOAL || two == WALL);
		boolean _1 = (one == BOX || one == BOX_ON_GOAL || one == WALL);

		if (five == BOX_ON_GOAL) {
			if ((eight == BOX_ON_GOAL || eight == WALL)
					&& (seven == BOX_ON_GOAL || seven == WALL)
					&& (four == BOX_ON_GOAL || four == WALL))
				return false;
			if ((eight == BOX_ON_GOAL || eight == WALL)
					&& (nine == BOX_ON_GOAL || nine == WALL)
					&& (six == BOX_ON_GOAL || six == WALL))
				return false;
			if ((four == BOX_ON_GOAL || four == WALL)
					&& (one == BOX_ON_GOAL || one == WALL)
					&& (two == BOX_ON_GOAL || two == WALL))
				return false;
			if ((two == BOX_ON_GOAL || two == WALL)
					&& (three == BOX_ON_GOAL || three == WALL)
					&& (six == BOX_ON_GOAL || six == WALL))
				return false;
		} else {
			if (_4 && _8 && _7)
				return true;
			if (_8 && _9 && _6)
				return true;
			if (_4 && _1 && _2)
				return true;
			if (_2 && _3 && _6)
				return true;
		}

		return false;
	}

	/**
	 * This method returns true if this state is locked, that is it's impossible
	 * to actually reach a goal.
	 * 
	 * @return true if the state is locked
	 */
	private boolean isWallDeadLock(int col, int row) {

		// We must allow these boards even if they do deadlock because they might be a necessary part of the solution.
		if(board[row][col] == BOX_ON_GOAL)
			return false;

		char _8 = board[row - 1][col];
		char _9 = board[row - 1][col + 1];
		char _7 = board[row - 1][col - 1];
		char _2 = board[row + 1][col];
		char _3 = board[row + 1][col + 1];
		char _1 = board[row + 1][col - 1];
		char _6 = board[row][col + 1];
		char _4 = board[row][col - 1];

		//System.out.println("Entering is wall deadlock");

		if (_8 == '#') {
			// We hit a wall, do some checks

			if (_4 == '#' || _6 == '#') {
				// Box is in a corner, DEADLOCK
				// System.out.println("We are in a corner");

				return true;
			}
			if (_7 == '#' && _9 == '#') {
				// System.out.println("Top left and top right is #");

				// Could be a deadlock, CHECK
				boolean left_wall = true;
				boolean right_wall = true;

				// System.out.println("Check for blocks to the left");
				// Check for blocking object to the left
				for (int i = col; i > 0; i--) {
					char _top_left = board[row - 1][i - 1];
					char _left = board[row][i - 1];
					char _top = board[row - 1][i];

					// System.out.println("top_left: " + _top_left + ", left: "
					//					 + _left + ", top: " + _top);

					if (_top != ' ' && _left == '#') {
						// System.out.println("We hit a wall to the left");
						break; // The box is surrounded by # to the left, BLOCK
					} else if (_top_left == ' ') {
						// System.out.println("We have a tunnel in the wall to the left");
						return false; // There is a tunnel in the wall to the
						// left, NO_BLOCK
					}
				}

				if (left_wall) {
					for (int i = col; i <= board[row].length; i++) {
						char _top_right = board[row - 1][i + 1];
						char _right = board[row][i + 1];
						char _top = board[row - 1][i];

						// System.out.println("top_right: " + _top_right +
						//						 ", right: " + _right + ", top: " + _top);

						if (_top != ' ' && _right == '#') {
							// System.out.println("We hit a wall to the right");
							break; // The box is surrounded by # to the right,
							// BLOCK
						} else if (_top_right == ' ') {
							// System.out.println("We have a tunnel in the wall to the right");
							return false; // There is a tunnel in the wall to
							// the right, NO_BLOCK
						}
					}
				}

				if (left_wall && right_wall) {
					int goals = 0;

					// System.out.println("innesluten, kolla antalet lådor och mål");
					for (int i = 0; i < board[row].length; i++) {
						char tile = board[row][i];

						if (tile == GOAL) {
							goals++;
						} else if (tile == BOX) {
							goals--; //H�r blir det lite knas va
						}
					}
					if (goals >= 0) {
						// System.out.println("MÅÅÅL");
						return false;
					}
					return true;
				}
			}
		}

		if (_2 == '#') {
			// We hit a wall, do some checks
			// System.out.println("Hit a wall to the bottom");

			if (_4 == '#' || _6 == '#') {
				// Box is in a corner, DEADLOCK
				// System.out.println("We are in a corner");

				return true;
			}
			if (_1 == '#' && _3 == '#') {
				// System.out.println("Bottom left and bottom right is #");

				// Could be a deadlock, CHECK
				boolean left_wall = true;
				boolean right_wall = true;

				// System.out.println("Check for blocks to the left");
				// Check for blocking object to the left
				for (int i = col; i > 0; i--) {
					char _bottom_left = board[row + 1][i - 1];
					char _left = board[row][i - 1];
					char _bottom = board[row + 1][i];

					// System.out.println("bottom_left: " + _bottom_left +
					//					 ", left: " + _left);

					if (_bottom != ' ' && _left == '#') {
						// System.out.println("We hit a wall to the left");
						break; // The box is surrounded by # to the left, BLOCK
					} else if (_bottom_left == ' ') {
						// System.out.println("We have a tunnel in the wall to the left");
						return false; // There is a tunnel in the wall to the
						// left, NO_BLOCK
					}
				}

				if (left_wall) {
					for (int i = col; i <= board[row].length; i++) {
						char _bottom_right = board[row + 1][i + 1];
						char _right = board[row][i + 1];
						char _bottom = board[row + 1][i];

						// System.out.println("bottom_right: " + _bottom_right +
						//						 ", right: " + _right + ", bottom: " + _bottom);

						if (_bottom != ' ' && _right == '#') {
							// System.out.println("We hit a wall to the right");
							break; // The box is surrounded by # to the right,
							// BLOCK
						} else if (_bottom_right == ' ') {
							// System.out.println("We have a tunnel in the wall to the right");
							return false; // There is a tunnel in the wall to
							// the right, NO_BLOCK
						}
					}
				}

				if (left_wall && right_wall) {

					int goals = 0;
					// System.out.println("innesluten, kolla antalet lådor och mål");
					for (int i = 0; i < board[row].length; i++) {
						char tile = board[row][i];

						if (tile == GOAL) {
							goals++;
						} else if (tile == BOX) {
							goals--;
						}
					}
					if (goals >= 0) {
						// System.out.println("MÅÅÅL");
						return false;
					}
					//					System.out.println("Down Dead");
					return true;
				}

			}
		}

		if (_6 == '#') {
			// We hit a wall, do some checks
			// System.out.println("Hit a wall to the right");

			if (_8 == '#' || _2 == '#') {
				// Box is in a corner, DEADLOCK
				// System.out.println("We are in a corner");

				return true;
			}
			if (_9 == '#' && _3 == '#') {
				// System.out.println("Top right and bottom right is #");

				// Could be a deadlock, CHECK
				boolean top_wall = true;
				boolean bottom_wall = true;

				// System.out.println("Check for blocks to the top");
				// Check for blocking object to the top
				for (int i = row; i > 0; i--) {
					char _top_right = board[i - 1][col + 1];
					char _right = board[i][col + 1];
					char _top = board[i - 1][col];

					// System.out.println("top_right: " + _top_right +
					//					 ", right: " + _right + ", top: " + _top);

					if (_right != ' ' && _top == '#') {
						// System.out.println("We hit a wall to the top");
						break; // The box is surrounded by # to the left, BLOCK
					} else if (_top_right == ' ') {
						// System.out.println("We have a tunnel in the wall to the top");
						return false; // There is a tunnel in the wall to the
						// left, NO_BLOCK
					}
				}

				if (top_wall) {
					for (int i = row; i <= board.length; i++) {
						char _bottom_right = board[i + 1][col + 1];
						char _right = board[i][col + 1];
						char _bottom = board[i + 1][col];

						// System.out.println("bottom_right: " + _bottom_right +
						//						 ", right: " + _right + ", bottom: " + _bottom);

						if (_right != ' ' && _bottom == '#') {
							// System.out.println("We hit a wall to the bottom");
							break; // The box is surrounded by # to the right,
							// BLOCK
						} else if (_bottom_right == ' ') {
							// System.out.println("We have a tunnel in the wall to the bottom");
							return false; // There is a tunnel in the wall to
							// the right, NO_BLOCK
						}
					}
				}

				if (top_wall && bottom_wall) {
					int goals = 0;
					// System.out.println("innesluten, kolla antalet lådor och mål");
					for (int i = 0; i < board.length; i++) {
						char tile = board[i][col];

						if (tile == GOAL) {
							goals++;
						} else if (tile == BOX) {
							goals--;
						}
					}
					if (goals >= 0) {
						// System.out.println("MÅÅÅL");
						return false;
					}
					return true;
				}
			}
		}

		if (_4 == '#') {
			// We hit a wall, do some checks
			// System.out.println("Hit a wall to the left");

			if (_8 == '#' || _2 == '#') {
				// Box is in a corner, DEADLOCK
				// System.out.println("We are in a corner");

				return true;
			}
			if (_7 == '#' && _1 == '#') {
				// System.out.println("Top left and bottom left is #");

				// Could be a deadlock, CHECK
				boolean top_wall = true;
				boolean bottom_wall = true;

				// System.out.println("Check for blocks to the top");
				// Check for blocking object to the top
				for (int i = row; i > 0; i--) {
					char _top_left = board[i - 1][col - 1];
					char _left = board[i][col - 1];
					char _top = board[i - 1][col];

					// System.out.println("top_left: " + _top_left + ", left: "
					//					 + _left + ", top: " + _top);

					if (_left != ' ' && _top == '#') {
						// System.out.println("We hit a wall to the top");
						break; // The box is surrounded by # to the left, BLOCK
					} else if (_top_left == ' ') {
						// System.out.println("We have a tunnel in the wall to the top");
						return false; // There is a tunnel in the wall to the
						// left, NO_BLOCK
					}
				}

				if (top_wall) {
					for (int i = row; i <= board.length; i++) {
						char _bottom_left = board[i + 1][col - 1];
						char _left = board[i][col - 1];
						char _bottom = board[i + 1][col];

						// System.out.println("bottom_left: " + _bottom_left +
						//						 ", left: " + _left + ", bottom: " + _bottom);

						if (_left != ' ' && _bottom == '#') {
							// System.out.println("We hit a wall to the bottom");
							break; // The box is surrounded by # to the right,
							// BLOCK
						} else if (_bottom_left == ' ') {
							// System.out.println("We have a tunnel in the wall to the bottom");
							return false; // There is a tunnel in the wall to
							// the right, NO_BLOCK
						}
					}
				}

				if (top_wall && bottom_wall) {
					int goals = 0;
					// System.out.println("innesluten, kolla antalet lådor och mål");
					for (int i = 0; i < board.length; i++) {
						char tile = board[i][col];

						if (tile == GOAL) {
							goals++;
						} else if (tile == BOX) {
							goals--;
						}
					}
					if (goals >= 0) {
						// System.out.println("MÅÅÅL");
						return false;
					}
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * This method returns true if this state solves the pussle. That is all
	 * boxes are located on a goal.
	 * 
	 * @return true if the puzzle is solved
	 */
	public boolean isSolved() {
		for (int r = 0; r < this.board.length; r++) {
			for (int c = 0; c < this.board[r].length; c++) {
				if (this.board[r][c] == BOX)
					return false;
			}
		}

		// System.out.println("Board was solved!");
		return true;
	}

	/**
	 * Returns a string representation of this board.
	 * 
	 * @return A new board. Null if the move was illegal.
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (int r = 0; r < this.board.length; r++) {
			for (int c = 0; c < this.board[r].length; c++) {
				sb.append((this.board[r][c]));
			}
			sb.append('\n');
		}

		return sb.toString();
	}

	/**
	 * Returns a string representation of this board.
	 * 
	 * @return A new board. Null if the move was illegal.
	 */
	public String toString(Coordinate highlight) {
		StringBuilder sb = new StringBuilder();

		for (int r = 0; r < this.board.length; r++) {
			for (int c = 0; c < this.board[r].length; c++) {
				if (highlight.getRow() == r && highlight.getColumn() == c) {
					sb.append("\033[31m" + this.board[r][c] + "\033[0m");
				} else {
					sb.append((this.board[r][c]));
				}
			}
			sb.append('\n');
		}

		return sb.toString();
	}

	/**
	 * Gets the current player position on the board
	 * 
	 * @param board
	 *            to be checked
	 * @return player coordinate
	 */
	public static Coordinate getPlayerPosition(Board b) {
		for (int r = 0; r < b.board.length; r++) {
			for (int c = 0; c < b.board[r].length; c++) {
				if (b.board[r][c] == PLAYER || b.board[r][c] == PLAYER_ON_GOAL || b.board[r][c] == PLAYER_ON_DEADLOCK) {
					return new Coordinate(c, r);
				}
			}
		}
		System.err.println("No player on board");
		System.exit(PLAYER);
		return null;
	}

	/**
	 * Gets all boxes on the current board, both those on a goal and those who
	 * are not.
	 * 
	 * @param Board
	 *            to be checked
	 * @return A list of the Coordinates on which the boxes are on.
	 */
	public static List<Coordinate> getAllBoxes(Board b) {
		List<Coordinate> l = new LinkedList<Coordinate>();
		for (int r = 0; r < b.board.length; r++) {
			for (int c = 0; c < b.board[r].length; c++) {
				if (b.board[r][c] == BOX || b.board[r][c] == BOX_ON_GOAL) {
					l.add(new Coordinate(c, r));
				}
			}
		}
		return l;
	}

	/**
	 * Gets all goals on the current board
	 * 
	 * @param Board
	 *            to be checked
	 * @return A list of the Coordinates where there are goals.
	 */
	public static List<Coordinate> getAllGoalsWithoutBox(Board b) {
		List<Coordinate> l = new LinkedList<Coordinate>();
		for (int r = 0; r < b.board.length; r++) {
			for (int c = 0; c < b.board[r].length; c++) {
				if (b.board[r][c] == GOAL || b.board[r][c] == PLAYER_ON_GOAL) {
					l.add(new Coordinate(c, r));
				}
			}
		}
		return l;
	}

	/**
	 * Gets all boxes that are on goals on the current board
	 * 
	 * @param Board
	 *            to be checked
	 * @return A list of the Coordinates on which the boxes are on.
	 */
	public static List<Coordinate> getAllBoxesOnGoals(Board b) {
		List<Coordinate> l = new LinkedList<Coordinate>();
		for (int r = 0; r < b.board.length; r++) {
			for (int c = 0; c < b.board[r].length; c++) {
				if (b.board[r][c] == BOX_ON_GOAL) {
					l.add(new Coordinate(c, r));
				}
			}
		}
		return l;
	}

	/**
	 * Gets all boxes that are not on goals on the current board
	 * 
	 * @param Board
	 *            to be checked
	 * @return A list of the Coordinates on which the boxes are on.
	 */
	public static List<Coordinate> getAllBoxesNotOnGoals(Board b) {
		List<Coordinate> l = new LinkedList<Coordinate>();
		for (int r = 0; r < b.board.length; r++) {
			for (int c = 0; c < b.board[r].length; c++) {
				if (b.board[r][c] == BOX) {
					l.add(new Coordinate(c, r));
				}
			}
		}
		return l;
	}

	/**
	 * Returns a hash volue of this board.
	 * 
	 * TODO: This function should be optimized.
	 * 
	 * @return hash of the board
	 */
	public int hashCode() {
		return this.toString().hashCode();
	}

	private Coordinate getTopLeftCoordinate(Coordinate player, char[][] temp) {
		int index_player_row = player.getRow();
		int index_player_column = player.getColumn();
		int index_above_player = player.getRow() - 1;
		int index_left_player = player.getColumn() -1;

		//While not both up and left is blocked
		while(!(temp[index_player_row][index_left_player] == BOX || temp[index_player_row][index_left_player] == BOX_ON_GOAL || temp[index_player_row][index_left_player] == WALL)
				&& (temp[index_above_player][index_player_column] == BOX || temp[index_above_player][index_player_column] == BOX_ON_GOAL || temp[index_above_player][index_player_column] == WALL)){
			//Try go up first
			if(temp[index_above_player][index_player_column] == BOX || temp[index_above_player][index_player_column] == BOX_ON_GOAL || temp[index_above_player][index_player_column] == WALL){
				if(temp[index_above_player][index_player_column] == SPACE){
					temp[index_above_player][index_player_column] = PLAYER;
					if(temp[index_player_row][index_player_column] == PLAYER){
						temp[index_player_row][index_player_column] = SPACE;
					} else{ //It was player on goal
						temp[index_player_row][index_player_column] = GOAL;
					}
				} else{ //IT IS GOAL
					temp[index_above_player][index_player_column] = PLAYER_ON_GOAL;
					if(temp[index_player_row][index_player_column] == PLAYER){
						temp[index_player_row][index_player_column] = SPACE;
					} else{ //It was player on goal
						temp[index_player_row][index_player_column] = GOAL;
					}
				}
				//UPDATE INDEXES SO PLAYER IS ONE ABOVE
				index_above_player --;
				index_player_row --;


			}
			//We must go left!
			else{
				if(temp[index_player_row][index_left_player] == SPACE){
					temp[index_player_row][index_left_player] = PLAYER;
					if(temp[index_player_row][index_player_column] == PLAYER){
						temp[index_player_row][index_player_column] = SPACE;
					} else{ //It was player on goal
						temp[index_player_row][index_player_column] = GOAL;
					}
				}	else{ //Left must be goal
					temp[index_player_row][index_left_player] = PLAYER_ON_GOAL;
					if(temp[index_player_row][index_player_column] == PLAYER){
						temp[index_player_row][index_player_column] = SPACE;
					} else{ //It was player on goal
						temp[index_player_row][index_player_column] = GOAL;
					}
				}

			}
			//UPDATE INDEXES SO PLAYER IS ONE LEFT
			index_left_player --;
			index_player_column --;

		}


		return new Coordinate(index_player_column, index_player_row);
	}

	/**
	 * Checks wether the player is standing next to a box or not.
	 * 
	 * @return true if the player i standing next to a box, false otherwise.
	 */
	public boolean playerNextToBox() {
		Coordinate player = getPlayerPosition(this);
		return coordinateNextToBox(player);
	}

	/**
	 * Heuristic function used to give a penalty to a board. This heurstic will
	 * give high penalties to boards where the user is far from any box.
	 * 
	 * @return int - A low penatly is good. MIN_VALUE means that the we are
	 *         standing next to a box. HIGH
	 */
	public int goToBoxHeuristic() {
		Coordinate player = getPlayerPosition(this);
		Coordinate nearestBox = new Coordinate(0, 0);
		List<Coordinate> boxCoords = new LinkedList<Coordinate>();
		int penalty = 0;

		nearestBox = getNearestBox(player, boxCoords);

		// If the player is close to the box, then we get a low penalty.

		for (int i = 0; i < boxCoords.size(); i++) {
			penalty += Math.abs(player.getRow() - nearestBox.getRow())
					+ Math.abs(player.getColumn() - nearestBox.getColumn());
		}

		return penalty;

	}

	/**
	 * This is the heuristic for the puzzle. It returns a penalty for this
	 * board. A <b>high</b> penalty means this is a <b>bad board</b>.
	 * 
	 * @return a penatly. <b>high is bad</b>
	 */
	public int puzzleHeuristic() {

		List<Coordinate> boxCoords = null;
		List<Coordinate> goalCoords = null;

		boxCoords = Board.getAllBoxesNotOnGoals(this);
		goalCoords = Board.getAllGoalsWithoutBox(this);
		Collections.reverse(goalCoords);

		int penalty = 0;

		// Have we solved the puzzle?
		if (this.isSolved())
			return Integer.MIN_VALUE;


		// ======= PLAYER TO NEAREST BOX DISTANCE ============
		Coordinate boxPos = null;
		Coordinate goalPos = null;

		Coordinate playerPos = getPlayerPosition(this);
		Coordinate nearestBox = getNearestBox(playerPos, boxCoords);

		int columnDist = Math.abs(playerPos.getColumn() - nearestBox.getColumn());
		int rowDist = Math.abs(playerPos.getRow() - nearestBox.getRow());

		penalty += Math.sqrt(Math.pow(columnDist, 2) + Math.pow(rowDist, 2));
		// ====================================================


		// ======= BOX TO FARTHEST EMPTY GOAL DISTANCE ========
		while(boxCoords.size() > 0) {
			boxPos = boxCoords.get(0);
			boxCoords.remove(0);

			goalPos = goalCoords.get(0);
			goalCoords.remove(0);

			//			List<Coordinate> room = getRoom(this);
			//			System.out.println("Room size: " + room.size());
			//			penalty += this.moveFromCoordinateToCoordinateUsingRoom(boxPos, goalPos, room).size();
			//			System.out.println("Penalty: " + penalty);
			int colsdist = Math.abs(boxPos.getColumn() - goalPos.getColumn());
			int rowdist = Math.abs(boxPos.getRow() - goalPos.getRow());

			penalty += Math.sqrt(Math.pow(colsdist, 2) + Math.pow(rowdist, 2));
		}
		// ====================================================

		// ============ NUMBER OF EMPTY GOALS =================
		for(int r = 0; r < this.board.length; r++){
			for(int c = 0; c < this.board[r].length; c++){
				if(this.board[r][c] == BOX_ON_GOAL){
					penalty -= 1;
				}
			}
		}
		// ====================================================

		//		System.out.println("Penalty after: " + penalty);

		return penalty;
	}

	// /**
	// * Gets the positions of all the boxes and stores them in boxCoords
	// * @param boxCoords An ArrayList of the coodrinate of all the boxes.
	// */
	// public void getBoxesPositions(ArrayList<Coordinate> boxCoords) {
	// for(int y = 0; y < this.board.length; y++) {
	// for(int x = 0; x < this.board[y].length; x++) {
	// if(this.board[y][x] == '$') {
	// boxCoords.add(new Coordinate(x,y));
	// }
	// }
	// }
	// }

	/**
	 * Gets the coordinates of the box that is closest to the player and stores
	 * them in the coordinate nearestBox.
	 * 
	 * @param start
	 *            Coordinate of the player
	 * @param nearestBox
	 *            The Coordinate we are searching for.
	 */
	public static Coordinate getNearestBox(Coordinate start, List<Coordinate> boxCoords) {
		Coordinate nearestBox = new Coordinate(Integer.MAX_VALUE,Integer.MAX_VALUE);
		int distanceToBox = 0;
		int minDistanceToBox = Integer.MAX_VALUE;
		for (Coordinate box : boxCoords) {
			int xBox = box.getColumn();
			int yBox = box.getRow();
			int xStart = start.getColumn();
			int yStart = start.getRow();

			// Calculate the "manhattan distance" to each box
			distanceToBox = Math.abs(xBox - xStart) + Math.abs(yBox - yStart);
			// Save the coordinate with the shortest distance from the player
			if (distanceToBox < minDistanceToBox) {
				minDistanceToBox = distanceToBox;
				nearestBox.setColumn(xBox);
				nearestBox.setRow(yBox);
			}
		}
		return nearestBox;
	}

	public static char[][] deepCopy(char[][] original) {
		if (original == null) {
			return null;
		}

		final char[][] result = new char[original.length][];
		for (int i = 0; i < original.length; i++) {
			result[i] = Arrays.copyOf(original[i], original[i].length);
			// For Java versions prior to Java 6 use the next:
			// System.arraycopy(original[i], 0, result[i], 0,
			// original[i].length);
		}
		return result;
	}

	public int playerGoalDistance() {
		Coordinate start = getPlayerPosition(this);
		int distanceToGoal = 0;
		List<Coordinate> goalCoords = getAllGoals();

		for(Coordinate goal : goalCoords){
			int xGoal = goal.getColumn();
			int yGoal = goal.getRow();
			int xStart = start.getColumn();
			int yStart = start.getRow();

			// Calculate the "manhattan distance" to each goal
			distanceToGoal += Math.abs(xGoal - xStart) + Math.abs(yGoal - yStart);
		}

		return distanceToGoal;
	}

	public List<Coordinate> getAllGoals(){
		List<Coordinate> l = new LinkedList<Coordinate>();
		for(int r = 0; r < this.board.length ; r++){
			for(int c = 0; c < this.board[r].length; c++){
				if(this.board[r][c] == GOAL){
					l.add(new Coordinate(c, r));
				}
			}
		}
		return l;
	}

	public static List<Coordinate> getRoom(Board board) {
		ArrayList<Integer> hashCodes = new ArrayList<Integer>();
		Coordinate startingCoordinate = getPlayerPosition(board);

		LinkedList<Coordinate> queue = new LinkedList<Coordinate>();
		LinkedList<Coordinate> room = new LinkedList<Coordinate>();
		queue.add(startingCoordinate);
		room.add(startingCoordinate);
		hashCodes.add(startingCoordinate.hashCode());

		//bfs
		int i = 1;
		while(!queue.isEmpty()){

			//			System.out.println("Iteration nr: " + i++);
			Coordinate currentCoordinate = queue.poll();
			//			System.out.println("Queue size == " + queue.size());
			//			System.out.println("Current Coordinate: " + currentCoordinate);


			if(canGoUpWithoutPush(board, currentCoordinate)){
				Coordinate nextCoordinate = new Coordinate(currentCoordinate, -1, 0);
				if(!hashCodes.contains(nextCoordinate.hashCode())){
					//					System.out.println("Add up" + nextCoordinate);
					queue.add(nextCoordinate);
					room.add(nextCoordinate);
					hashCodes.add(nextCoordinate.hashCode());
				}
			}
			if(canGoDownWithoutPush(board, currentCoordinate)){
				Coordinate nextCoordinate = new Coordinate(currentCoordinate, 1, 0);
				if(!hashCodes.contains(nextCoordinate.hashCode())){
					//				System.out.println("Add down" + nextCoordinate);
					queue.add(nextCoordinate);
					room.add(nextCoordinate);
					hashCodes.add(nextCoordinate.hashCode());
				}
			}
			if(canGoLeftWithoutPush(board, currentCoordinate)){
				Coordinate nextCoordinate = new Coordinate(currentCoordinate, 0, -1);
				if(!hashCodes.contains(nextCoordinate.hashCode())){
					//					System.out.println("Add left" + nextCoordinate);
					queue.add(nextCoordinate);
					room.add(nextCoordinate);
					hashCodes.add(nextCoordinate.hashCode());
				}
			}
			if(canGoRightWithoutPush(board, currentCoordinate)){
				Coordinate nextCoordinate = new Coordinate(currentCoordinate, 0, 1);
				if(!hashCodes.contains(nextCoordinate.hashCode())){
					//					System.out.println("Add right" + nextCoordinate);
					queue.add(nextCoordinate);
					room.add(nextCoordinate);
					hashCodes.add(nextCoordinate.hashCode());
				}
			}
			//			System.out.println("\n");



		}
		//		for (Coordinate c : room){
		//			System.out.println(c);
		//		}
		return room;
	}

	private static boolean canGoUpWithoutPush(Board board, Coordinate player){
		if(board.board[player.getRow()-1][player.getColumn()] == BOX_ON_GOAL || board.board[player.getRow()-1][player.getColumn()] == BOX || board.board[player.getRow()-1][player.getColumn()] == WALL){
			return false;
		} else{
			return true;
		}
	}

	private static boolean canGoDownWithoutPush(Board board, Coordinate player){
		if(board.board[player.getRow()+1][player.getColumn()] == BOX_ON_GOAL || board.board[player.getRow()+1][player.getColumn()] == BOX || board.board[player.getRow()+1][player.getColumn()] == WALL){
			return false;
		} else{
			return true;
		}
	}

	private static boolean canGoLeftWithoutPush(Board board, Coordinate player){
		if(board.board[player.getRow()][player.getColumn()-1] == BOX_ON_GOAL || board.board[player.getRow()][player.getColumn()-1] == BOX || board.board[player.getRow()][player.getColumn()-1] == WALL){
			return false;
		} else{
			return true;
		}
	}

	private static boolean canGoRightWithoutPush(Board board, Coordinate player){
		if(board.board[player.getRow()][player.getColumn()+1] == BOX_ON_GOAL || board.board[player.getRow()][player.getColumn()+1] == BOX || board.board[player.getRow()][player.getColumn()+1] == WALL){
			return false;
		} else{
			return true;
		}
	}

	public static char getChar(char[][] array, Coordinate coordinate){
		return array[coordinate.getRow()][coordinate.getColumn()];
	}

	public void swapCoordinates(Coordinate first, Coordinate second) {
		char temp = board[first.getRow()][first.getColumn()];
		board[first.getRow()][first.getColumn()] = board[second.getRow()][second.getColumn()];
		board[second.getRow()][second.getColumn()] = temp;
	}

	public boolean canMoveBoxUp(Coordinate c) {
		int row = c.getRow();
		int column = c.getColumn();
		char oneUp = board[row-1][column];
		if(oneUp == WALL || oneUp == SPACE || oneUp == GOAL ){
			return false;
		}
		char twoUp = board[row-2][column];
		if(oneUp == BOX || oneUp == BOX_ON_GOAL){
			return !(twoUp ==
					BOX || twoUp == BOX_ON_GOAL || twoUp == WALL);
		}
		return false;
	}

	public boolean canMoveBoxDown(Coordinate c) {
		int row = c.getRow();
		int column = c.getColumn();
		char oneDown = board[row+1][column];
		if(oneDown == WALL|| oneDown == SPACE || oneDown == GOAL){
			return false;
		}
		char twoDown = board[row+2][column];
		if(oneDown == BOX || oneDown == BOX_ON_GOAL){
			return !(twoDown == BOX || twoDown == BOX_ON_GOAL || twoDown == WALL);
		}
		return true;
	}

	public boolean canMoveBoxLeft(Coordinate c) {
		int row = c.getRow();
		int column = c.getColumn();
		char oneLeft = board[row][column-1];
		if(oneLeft == WALL|| oneLeft == SPACE || oneLeft == GOAL){
			return false;
		}
		char twoLeft = board[row][column-2];
		if(oneLeft == BOX || oneLeft == BOX_ON_GOAL){
			return !(twoLeft == BOX || twoLeft == BOX_ON_GOAL || twoLeft == WALL);
		}
		return true;
	}

	public boolean canMoveBoxRight(Coordinate c) {
		int row = c.getRow();
		int column = c.getColumn();
		char oneRight = board[row][column+1];
		if(oneRight == WALL|| oneRight == SPACE || oneRight == GOAL){
			return false;
		}
		char twoRight = board[row][column+2];
		if(oneRight == BOX || oneRight == BOX_ON_GOAL){
			return !(twoRight == BOX || twoRight == BOX_ON_GOAL || twoRight == WALL);
		}
		return true;
	}

	public boolean coordinateNextToBox(Coordinate c) {
		int column = c.getColumn();
		int row = c.getRow();
		return (board[row][column - 1] == BOX
				|| board[row][column + 1] == BOX 
				|| board[row + 1][column] == BOX
				|| board[row - 1][column] == BOX
				|| board[row][column + 1] == BOX_ON_GOAL
				|| board[row][column - 1] == BOX_ON_GOAL
				|| board[row + 1][column] == BOX_ON_GOAL
				|| board[row - 1][column] == BOX_ON_GOAL);
	}

	public LinkedList<Character> moveFromCoordinateToCoordinateUsingRoom(
			Coordinate start, Coordinate goal, List<Coordinate> room) {


		if(start.equals(goal)){
			return new LinkedList<Character>();
		}

		ArrayList<Integer> hashCodes = new ArrayList<Integer>();
		LinkedList<Coordinate> queue = new LinkedList<Coordinate>();
		LinkedList<LinkedList<Character>> paths = new LinkedList<LinkedList<Character>>();
		queue.add(start);
		hashCodes.add(start.hashCode());
		paths.add(new LinkedList<Character>());

		//BFS
		while(!queue.isEmpty()){
			Coordinate currentCoordinate = queue.poll();
			LinkedList<Character> currentPath = paths.poll();

			Coordinate upCoordinate = new Coordinate(currentCoordinate, -1,0);
			Coordinate downCoordinate = new Coordinate(currentCoordinate, 1,0);
			Coordinate leftCoordinate = new Coordinate(currentCoordinate, 0,-1);
			Coordinate rightCoordinate = new Coordinate(currentCoordinate, 0,1);

			if(room.contains(upCoordinate) && !hashCodes.contains(upCoordinate.hashCode())){
				LinkedList<Character> nextPath = (LinkedList<Character>) currentPath.clone();
				nextPath.add('U');
				paths.add(nextPath);
				hashCodes.add(upCoordinate.hashCode());
				queue.add(upCoordinate);
				if(upCoordinate.equals(goal)){
					return nextPath;
				}
			}

			if(room.contains(downCoordinate) && !hashCodes.contains(downCoordinate.hashCode())){
				LinkedList<Character> nextPath = (LinkedList<Character>) currentPath.clone();
				nextPath.add('D');
				paths.add(nextPath);
				hashCodes.add(downCoordinate.hashCode());
				queue.add(downCoordinate);
				if(downCoordinate.equals(goal)){
					return nextPath;
				}
			}

			if(room.contains(leftCoordinate) && !hashCodes.contains(leftCoordinate.hashCode())){
				LinkedList<Character> nextPath = (LinkedList<Character>) currentPath.clone();
				nextPath.add('L');
				paths.add(nextPath);
				hashCodes.add(leftCoordinate.hashCode());
				queue.add(leftCoordinate);
				if(leftCoordinate.equals(goal)){
					return nextPath;
				}
			}

			if(room.contains(rightCoordinate) && !hashCodes.contains(rightCoordinate.hashCode())){
				LinkedList<Character> nextPath = (LinkedList<Character>) currentPath.clone();
				nextPath.add('R');
				paths.add(nextPath);
				hashCodes.add(rightCoordinate.hashCode());
				queue.add(rightCoordinate);
				if(rightCoordinate.equals(goal)){
					return nextPath;
				}
			}


		}
		System.err.println("No Goal");
		System.exit(213123);
		return null;
	}



	public boolean safeswapCoordinates(Coordinate first,
			Coordinate second) {
		char firstChar = board[first.getRow()][first.getColumn()];
		char secondChar = board[second.getRow()][second.getColumn()];
		if(isInvalidSafeSwapChar(firstChar) || isInvalidSafeSwapChar(secondChar)){
			return false;
		}
		if(firstChar == secondChar){
			return true;
		}
		if(firstChar == SPACE){
			if(isSafeChar(secondChar)){
				swapCoordinates(first, second);
				return true;
			}
			else if(secondChar == GOAL){
				return true;				
			} 
			else if(secondChar == PLAYER_ON_GOAL){
				board[first.getRow()][first.getColumn()] = PLAYER;
				board[second.getRow()][second.getColumn()] = GOAL;
				return true;
			}
		}
		else if(firstChar == GOAL){
			if(secondChar == PLAYER){
				board[first.getRow()][first.getColumn()] = PLAYER_ON_GOAL;
				board[second.getRow()][second.getColumn()] = SPACE;
				return true;				
			}
			else if(secondChar == PLAYER_ON_GOAL){
				swapCoordinates(first, second);
				return true;
			}
			else if(secondChar == SPACE){
				return true;
			}
		} 
		else if(firstChar == PLAYER_ON_GOAL){
			if(secondChar == SPACE){
				board[first.getRow()][first.getColumn()] = GOAL;
				board[second.getRow()][second.getColumn()] = PLAYER;
				return true;
			} else if(secondChar == GOAL){
				swapCoordinates(first, second);
				return true;
			}
		}
		else if(firstChar == PLAYER){
			if(secondChar == SPACE){
				swapCoordinates(first, second);
				return true;
			}
			else if(secondChar ==GOAL ){
				board[first.getRow()][first.getColumn()] = SPACE;
				board[second.getRow()][second.getColumn()] = PLAYER_ON_GOAL;
				return true;
			}
		}
		return false;
	}	

	private boolean isSafeChar(char c){
		return c == PLAYER || c == SPACE;
	}
	private boolean isInvalidSafeSwapChar(char c){
		return c == WALL || c == BOX || c == BOX_ON_GOAL;
	}

	static public char[][] generateDummyBoard(int cols, int rows) {

		if(cols < 4 || rows < 11) {
			System.out.println("Error! Make the board atleast 5x12");
			System.exit(1);
		}

		char[][] board = new char[rows][cols];

		for(int r = 0; r < rows; r++) {
			for(int c = 0; c < cols; c++) {
				board[r][c] = ' ';
			}
		}

		for(int r = 0; r < rows; r++) {
			board[r][0] = '#';
			board[r][cols -1] = '#';
		}

		for(int c = 0; c < cols; c++) {
			board[0][c] = '#';
			board[rows - 1][c] = '#';
		}

		board[4][4] = '.';
		board[3][4] = '$';
		board[1][4] = '@';
		board[3][3] = '#';
		board[4][3] = '#';
		board[5][3] = '#';
		board[3][5] = '#';
		board[4][5] = '#';
		board[5][5] = '#';
		board[5][4] = '#';


		board[4][8] = '.';
		board[3][8] = '$';
		board[3][7] = '#';
		board[4][7] = '#';
		board[5][7] = '#';
		board[3][9] = '#';
		board[4][9] = '#';
		board[5][9] = '#';
		board[5][8] = '#';

		return board;
	}

	static public char[][] readFromFile(String filename) {
		System.out.println("Reading from file: " + filename);
		char[][] board = null;
		ArrayList<String> rows = new ArrayList<String>();

		try {
			BufferedReader in = new BufferedReader(new FileReader(filename));

			String row;
			while((row = in.readLine()) != null) {
				rows.add(row);
			}

			in.close();
		} catch(IOException e) {
			e.printStackTrace();
		}

		System.out.println("Read " + rows.size() + " rows.");

		board = new char[rows.size()][];

		for(int i = 0; i < rows.size(); i++) {
			board[i] = rows.get(i).toCharArray();
		}

		return board;
	}

}