public class DeadlockChecker {	

	public static char[][] board = new char[][]{
		{'#', '#', '#', '#', '#', '#', '#'},
		{'#', ' ', ' ', ' ', ' ', ' ', '#'},
		{'#', ' ', ' ', '*', '*', ' ', '#'},
		{'#', ' ', ' ', '*', '*', ' ', '#'},
		{'#', ' ', ' ', '$', '*', ' ', '#'},
		{'#', ' ', ' ', ' ', ' ', ' ', '#'},
		{'#', ' ', ' ', ' ', ' ', ' ', '#'},
		{'#', '#', '#', '#', '#', '#', '#'}
	};

	public static final char SPACE = ' ';
	public static final char BOX = '$';
	public static final char WALL = '#';
	public static final char GOAL = '.';
	public static final char BOX_ON_GOAL = '*';
	public static final char PLAYER = '@';
	public static final char PLAYER_ON_GOAL = '+';

	private static boolean isSquareDeadLock(int col, int row) {
		System.out.println(board[row][col]);

		char eight 	= board[row - 1][col];
		char nine	= board[row - 1][col + 1];
		char seven	= board[row - 1][col - 1];
		char two 	= board[row + 1][col];
		char five 	= board[row][col];
		char three 	= board[row + 1][col + 1];
		char one	= board[row + 1][col - 1];
		char six 	= board[row][col + 1];
		char four 	= board[row][col - 1];

		boolean _9 	= (nine == BOX || nine == BOX_ON_GOAL || nine == WALL);
		boolean _8 	= (eight == BOX || eight == BOX_ON_GOAL || eight == WALL);
		boolean _7 	= (seven == BOX || seven == BOX_ON_GOAL || seven == WALL);
		boolean _6 	= (six == BOX || six == BOX_ON_GOAL || six == WALL);
		boolean _5 	= (five == BOX || five == BOX_ON_GOAL || five == WALL);
		boolean _4 	= (four == BOX || four == BOX_ON_GOAL || four == WALL);
		boolean _3 	= (three == BOX || three == BOX_ON_GOAL || three == WALL);
		boolean _2 	= (two == BOX || two == BOX_ON_GOAL || two == WALL);
		boolean _1 	= (one == BOX || one == BOX_ON_GOAL || one == WALL);

		if(five == BOX_ON_GOAL){
			if((eight == BOX_ON_GOAL || eight == WALL) && (seven == BOX_ON_GOAL || seven == WALL) && (four == BOX_ON_GOAL || four == WALL))
				return false;
			if((eight == BOX_ON_GOAL || eight == WALL) && (nine == BOX_ON_GOAL || nine == WALL) && (six == BOX_ON_GOAL || six == WALL))
				return false;
			if((four == BOX_ON_GOAL || four == WALL) && (one == BOX_ON_GOAL || one == WALL) && (two == BOX_ON_GOAL || two == WALL))
				return false;
			if((two == BOX_ON_GOAL || two == WALL) && (three == BOX_ON_GOAL || three == WALL) && (six == BOX_ON_GOAL || six == WALL))
				return false;
		}
		else {
			if(_4 && _8 && _7)
				return true;
			if(_8 && _9 && _6)
				return true;
			if(_4 && _1 && _2)
				return true;
			if(_2 && _3 && _6)
				return true;
		}

		return false;
	}

	private static boolean isTetrisDeadLock(int col, int row) {
		System.out.println(board[row][col]);

		char eight 	= board[row - 1][col];
		char nine	= board[row - 1][col + 1];
		char seven	= board[row - 1][col - 1];
		char two 	= board[row + 1][col];
		char five 	= board[row][col];
		char three 	= board[row + 1][col + 1];
		char one	= board[row + 1][col - 1];
		char six 	= board[row][col + 1];
		char four 	= board[row][col - 1];

		boolean _9 	= (nine == BOX || nine == BOX_ON_GOAL || nine == WALL);
		boolean _8 	= (eight == BOX || eight == BOX_ON_GOAL || eight == WALL);
		boolean _7 	= (seven == BOX || seven == BOX_ON_GOAL || seven == WALL);
		boolean _6 	= (six == BOX || six == BOX_ON_GOAL || six == WALL);
		boolean _5 	= (five == BOX || five == BOX_ON_GOAL || five == WALL);
		boolean _4 	= (four == BOX || four == BOX_ON_GOAL || four == WALL);
		boolean _3 	= (three == BOX || three == BOX_ON_GOAL || three == WALL);
		boolean _2 	= (two == BOX || two == BOX_ON_GOAL || two == WALL);
		boolean _1 	= (one == BOX || one == BOX_ON_GOAL || one == WALL);

		if(five == BOX_ON_GOAL) {
			if((two == SPACE && eight == BOX_ON_GOAL) || (four == SPACE && six == BOX_ON_GOAL) || (eight == SPACE && two == BOX_ON_GOAL) || (six == SPACE && four == BOX_ON_GOAL)) {
				return false;
			}
		}
		else {
			if(two == SPACE && _8) {
				if((four == WALL && nine == WALL) || (six == WALL && seven == WALL)) 
					return true;
			}
			else if(eight == SPACE && _2) {
				if((four == WALL && three == WALL) || (six == WALL && one == WALL))
					return true;
			}
			else if(four == SPACE && _6) {
				if((two == WALL && nine == WALL) || (eight == WALL && three == WALL))
					return true;
			}
			else if(six == SPACE && _4){
				if((eight == WALL && one == WALL) || (seven == WALL && two == WALL))
					return true;
			}
		}

		return false;
	}

	public static void main(String[] pArgs) {
		boolean bool = isTetrisDeadLock(3, 4) || isSquareDeadLock(3, 4);

		if(bool)
			System.out.println("Deadlock");
		else
			System.out.println("NOT deadlock");

	}
}