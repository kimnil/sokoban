import java.util.List;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class Client {

	static char board[][] = null;
	
	public static Socket lSocket;
	public static PrintWriter lOut;
	public static BufferedReader lIn;

	private static byte iter;

	public static void main(String[] pArgs) 
	{
		
		if(pArgs.length<3)
		{
			System.out.println("usage: java Client host port boardnum");
			return;
		}
	
		try
		{
			lSocket=new Socket(pArgs[0],Integer.parseInt(pArgs[1]));
			lOut=new PrintWriter(lSocket.getOutputStream());
			lIn=new BufferedReader(new InputStreamReader(lSocket.getInputStream()));
	
            lOut.println(pArgs[2]);
            lOut.flush();

            String lLine=lIn.readLine();

            //read number of rows
            int lNumRows=Integer.parseInt(lLine);
           
            board = new char[lNumRows][];

            //read each row
            for(int i=0;i<lNumRows;i++)
            {
                board[i] = lIn.readLine().toCharArray();
                //here, we would store the row somewhere, to build our board
                //in this demo, we just print it
                //System.out.println(board[i]);
            }

            //board = Board.generateDummyBoard(23, 15);
            //board = Board.readFromFile("mix2.txt");

            
			/**
			 * This Queue holds all the states that will be our search tree. The queue will be sorted by
			 * the penalty of the states. The queues <b>head will have the lowest</b> penalty.
			 */
		    PriorityQueue<State> queue = new PriorityQueue<State>();
		    
		    /**
		     * This list will contain the hash values of all the boards that we have ever have had in the queue.
		     * This is so we don't repeat states.
		     */
		    ArrayList<Integer> visitedBoards = new ArrayList<Integer>();
            
            State initial_state = new State();
            LinkedList<Character> path = new LinkedList<Character>();
            path.add('\n');
           
            initial_state.path = path;
            initial_state.prev = null;
            initial_state.setBoard(new Board(board));
            //initial_state.getBoard().markWallDeadlocks();

			System.out.println("TEST BOARD: " + initial_state.toString());

            System.out.println("Board looks like: ");
            System.out.println(initial_state.toString());
        	System.out.println("Press the any-key to go!");
        	try {
        		System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
        	
            /*
            // Will start the search for a solution at each box once, until
            // a solution is found. 
        	nearestBox = Board.getNearestBox(Board.getPlayerPosition(initial_state.getBoard()), boxCoords);
        	stateNextToBox = goToBox(initial_state, nearestBox);
          
        	if(stateNextToBox == null) {
        		System.out.println("Couldn't reach any box.");
        		System.exit(1);
        	}
            System.out.println("We have reached a box!");
            
           	stateNextToBox.addStatesIntoQueue(queue, visitedBoards);

           	*/
			queue.add(initial_state);
			
            State state = aStar(queue, visitedBoards);
            /*
            while(!queue.isEmpty()) {
            	
            	state = queue.poll();
        		System.out.println(state.toString());
        		
            	if(state.getBoard().isSolved()) {
            		System.out.println("Solution found!");
            		System.exit(0);
            	} else {
            		state.addStatesIntoQueue(queue, visitedBoards);
            	}
            } 
            */
           
            if (state != null) {
	            System.out.println("Goal: \n" + state);
	            state.sendPath(lOut, lIn);
            } else {
            	System.out.println("No goal was found.");
            }
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
	/**
	 * Takes the player to the nearest box using the A* algorithm.
	 * @param state initial state
	 * @param coord Coordinate to the nearest box
	 * @return A state where the player is next to a box.
	 */
	/*private static State goToBox(State state, Coordinate boxCoord){
		PriorityQueue<State> queue = new PriorityQueue<State>();
		ArrayList<Integer> visitedBoards = new ArrayList<Integer>();
		
		System.out.println("Walking to box at " + boxCoord.toString());
		
		queue.add(state);
		State s = null;
		int i = 0;
		while(!queue.isEmpty()){
			s = queue.poll();
			
			System.out.println("i: " + i);
			i++;
        	// Print the current board and prompt for user input to continue.
        	System.out.println(s.toString());
        	try {
        		System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			visitedBoards.add(s.hashCode());
			
			Coordinate player = Board.getPlayerPosition(s.getBoard());
			// Left or right...
			if(player.getRow() == boxCoord.getRow() && (player.getColumn() == boxCoord.getColumn()-1 || player.getColumn() == boxCoord.getColumn() + 1)) {
				return s;
			}
			
			// ...below or above... 
			else if(player.getColumn() == boxCoord.getColumn() && (player.getRow() == boxCoord.getRow()-1 || player.getRow() == boxCoord.getRow() + 1)) {
				return s;
			} 
		
			// ... not standing next to box.
			else {
				s.addStatesIntoQueue(queue, visitedBoards);
			}
        	System.out.println("---------------------------------------------------------");
		}
		
		return null;
	}*/

	/**
	 * Searches for a State where all the goals have boxes on them.
	 * @param queue A priority queue of states to check next. States with high value (low score)
	 * 				 are in the head of the queue and are checked first.
	 * @param visitedBoards A list of hashvalues of the visited boards
	 * @return Returns a state with a finished goal.
	 */
	private static State aStar(PriorityQueue<State> queue, ArrayList<Integer> visitedBoards) {
        State state = null;
        
        System.out.println("A* Commencing");
        
        while(!queue.isEmpty()) {
        	state = queue.poll();
        	
        	// Print the current board and prompt for user input to continue.
        	/*
        	System.out.println("Just polled:");
        	System.out.println(state.toString());
        	
        	try {
        		System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
        	
			*/

        	if(iter++ == Byte.MAX_VALUE){
        		System.out.println(state.toString());
	        	System.out.println("Queue size == " + queue.size());
        	}

        	
        	if (state.getBoard().isSolved()) {
        		System.out.println("Solution found!");
        		return state;
        	} else {
//        		System.out.println("Queueing neighbours.");
        		state.addStatesIntoQueue(queue, visitedBoards);
        		//System.out.println("Queue size: " + queue.size());
        	}
        	//System.out.println("---------------------------------------------------------");
        }
        
        return null;
	}
	
	/**
	 * Gets the coordinates of the box that is closest to the player and stores them 
	 * in the coordinate nearestBox.
	 * @param start Coordinate of the player
	 * @param nearestBox The Coordinate we are searching for.
	 */
	public static void getNearestBox(Coordinate start, Coordinate nearestBox,
										ArrayList<Coordinate> boxCoords) {
        int distanceToBox = 0;
        int minDistanceToBox = Integer.MAX_VALUE;
        for(Coordinate box : boxCoords){
        	int xBox = box.getRow();
        	int yBox = box.getColumn();
        	int xStart = start.getRow();
        	int yStart = start.getColumn();
        	
        	// Calculate the "manhattan distance" to each box
        	distanceToBox = Math.abs(xBox - xStart) + Math.abs(yBox - yStart);
        	// Save the coordinate with the shortest distance from the player
        	if(distanceToBox < minDistanceToBox){
        		minDistanceToBox = distanceToBox;
        		nearestBox.setColumn(xBox);
        		nearestBox.setRow(yBox);
        		
        		// Remove it from the ArrayList, we don't want to start the search
        		// from this box again
        		boxCoords.remove(box);
        	}
        }
	}
}
