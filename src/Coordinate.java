
public class Coordinate {

	private int x;
	private int y;
	
	public Coordinate(int x2, int y2) {
		x = x2;
		y = y2;
	}
	
	public Coordinate(Coordinate oldCoordinate, int rowDifference, int ColumnDifferrence){
		y = oldCoordinate.getRow() + rowDifference;
		x = oldCoordinate.getColumn() + ColumnDifferrence;
	}
	
	public int getColumn() {
		return x;
	}
	public void setColumn(int x) {
		this.x = x;
	}
	public int getRow() {
		return y;
	}
	public void setRow(int y) {
		this.y = y;
	}
	
	public String toString() {
		return "Row: " + y + " Column: " + x; 
	}
	
	public boolean equals(Coordinate other) {
		return this.x  == other.x && this.y == other.y;
	}
	
	public int hashCode(){
		return (x*1024)+y;
	}
	
	public boolean equals(Object other) {
		return this.x  == ((Coordinate)other).x && this.y == ((Coordinate)other).y;
	}
	
}
