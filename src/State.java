import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
 * A state represents the current state of the sokoban board. 
 */
public class State implements Comparable<State> {

	public char type;

	// The complete path to get to this state.
	public LinkedList<Character> path;

	// Current position of the player. This might be unnecessary in the sokoban project.
	private Coordinate pos;

	// This is a pointer to the previous step so that we can return our path later on.
	public State prev;

	// The gameboard of this state.
	public Board board;

	// Scanner to read input
	private Scanner scanner = new Scanner(System.in);

	/**
	 * String representation of this state.
	 */
	public String toString() {

		if(this.getBoard() == null) {
			//System.out.println("board is null");
		}
		StringBuilder sb = new StringBuilder();

		sb.append(getBoard().toString());

		// ... and some additional state information.
		sb.append("Player position X: " + Board.getPlayerPosition(this.getBoard()).getColumn() + " Y: " + Board.getPlayerPosition(this.getBoard()).getRow());
		sb.append(". Action to get here: " + this.path.getLast() + " (size " + path.size() + ")");

		return sb.toString();
	}
	
	/**
	 * String representation of this state with a given coordinate highlighted.
	 * @param coord highlight this coord
	 */
	public String toString(Coordinate highlight) {

		if(this.getBoard() == null) {
			//System.out.println("board is null");
		}
		StringBuilder sb = new StringBuilder();

		sb.append(getBoard().toString(highlight));

		// ... and some additional state information.
		sb.append("Player position X: " + Board.getPlayerPosition(this.getBoard()).getColumn() + " Y: " + Board.getPlayerPosition(this.getBoard()).getRow());
		sb.append(". Action to get here: " + this.path.getLast() + " (size " + path.size() + ")");

		return sb.toString();
	}



	/**
	 * This method creates a path to how we got to this state and sends it to the server.
	 */
	public void sendPath(PrintWriter lOut, BufferedReader in) throws Exception {
		StringBuilder sol = new StringBuilder();

//		for(int i = 0; i <= path.size(); i++){
//			sol.append(path.getFirst().toString());
//			path.removeFirst();
//		}
		for(char c : path){
			sol.append(c);
		}

		System.out.println("Path to solution: " + sol.toString().substring(1) + ".");
		//send the solution to the server
		lOut.println(sol.toString().substring(1));
		lOut.flush();

		//read answer from the server
		String lLine = "";
		lLine=in.readLine();

		System.out.println(lLine);
	}

	/**
	 * This method expands (generates all possible moves) this state and puts them into a supplied queue.
	 */
	@SuppressWarnings("unchecked")
	public void addStatesIntoQueue(PriorityQueue<State> queue, ArrayList<Integer> visitedBoards) {
		boolean PRINT_DEADLOCK = false;
		List<Coordinate> room = Board.getRoom(this.getBoard());
		Coordinate topLeft = new Coordinate(Integer.MAX_VALUE, Integer.MAX_VALUE);

		for (Coordinate c : room){
			if(c.getRow() <= topLeft.getRow() && c.getColumn() < topLeft.getColumn()){
				topLeft = c;
			}
		}

		Coordinate playerPosition = Board.getPlayerPosition(this.board);
		
		LinkedList<Character> moves = board.moveFromCoordinateToCoordinateUsingRoom(playerPosition, topLeft, room);
		
		this.path.addAll(moves);

		this.board.safeswapCoordinates(playerPosition, topLeft);
		visitedBoards.add(new Integer(this.hashCode()));

		//Check if pushable box in all directions

		for (Coordinate c : room){

			if(board.coordinateNextToBox(c)){

				if(board.canMoveBoxUp(c)){
					//					System.out.println("Entering move up");
					State newState = new State();
					newState.setBoard(board.copy());
					newState.path = (LinkedList<Character>) this.path.clone();
					newState.path.addAll(newState.board.moveFromCoordinateToCoordinateUsingRoom(topLeft, c, room));
					
					newState.board.safeswapCoordinates(c, topLeft);
					newState.board = newState.board.goUp();
					if(newState.board != null){
						Coordinate nextCoordinate = new Coordinate(c, -1, 0);
						boolean safeSwap = newState.board.safeswapCoordinates(nextCoordinate, topLeft);
						
						// ======= DEADLOCK PRINT =========
						if(PRINT_DEADLOCK){
							if(newState.board.isDeadLock(getBoxDiff(this, newState))){
									System.out.println("=======DEADLOCK========");
									System.out.println(newState.toString(getBoxDiff(this, newState)));
									try {
						        		System.in.read();
									} catch (IOException e) {
										e.printStackTrace();
									}
							}
						}

						// If the board is solved we don't care if its a deadlock!
						if(newState.getBoard().isSolved()) {
							queue.add(newState);
						}
						
						if(safeSwap && !newState.board.isDeadLock(getBoxDiff(this, newState)) && !visitedBoards.contains(newState.hashCode())){
													//System.out.println("Added Up");
							visitedBoards.add(newState.hashCode());
							newState.board.safeswapCoordinates(nextCoordinate, topLeft);
							newState.path.add('U');
							queue.add(newState);
						}

					}
				}

				if(board.canMoveBoxDown(c)){
					//					System.out.println("Entering move down");

					State newState = new State();
					newState.setBoard(board.copy());
					newState.path = (LinkedList<Character>) this.path.clone();
					newState.path.addAll(newState.board.moveFromCoordinateToCoordinateUsingRoom(topLeft, c, room));
					newState.board.safeswapCoordinates(c, topLeft);
					newState.board = newState.board.goDown();
					if(newState.board != null){
						Coordinate nextCoordinate = new Coordinate(c, 1, 0);
						boolean safeSwap =newState.board.safeswapCoordinates(nextCoordinate, topLeft);
	
						// ======= DEADLOCK PRINT =========
						if(PRINT_DEADLOCK){
	
							if(newState.board.isDeadLock(getBoxDiff(this, newState))){
									System.out.println("=======DEADLOCK========");
									System.out.println(newState.toString(getBoxDiff(this, newState)));
									try {
						        		System.in.read();
									} catch (IOException e) {
										e.printStackTrace();
									}
							}
						}
						
						// If the board is solved we don't care if its a deadlock!
						if(newState.getBoard().isSolved()) {
							queue.add(newState);
						}
						
						if(safeSwap && !visitedBoards.contains(newState.hashCode())){
												//System.out.println("Added Downt");
							visitedBoards.add(newState.hashCode());
							newState.board.safeswapCoordinates(nextCoordinate, topLeft);
							if(!newState.board.isDeadLock(getBoxDiff(this, newState))){
								queue.add(newState);
								newState.path.add('D');
	
							}

						}
					}
				}

				if(board.canMoveBoxLeft(c)){
					//					System.out.println("Entering move left");
					State newState = new State();
					newState.setBoard(board.copy());
					newState.path = (LinkedList<Character>) this.path.clone();
					newState.path.addAll(newState.board.moveFromCoordinateToCoordinateUsingRoom(topLeft, c, room));
					newState.board.safeswapCoordinates(c, topLeft);
					newState.board = newState.board.goLeft();
					if(newState.board != null){
						Coordinate nextCoordinate = new Coordinate(c, 0, -1);
						boolean safeSwap =newState.board.safeswapCoordinates(nextCoordinate, topLeft);
	
						// ======= DEADLOCK PRINT =========
						if(PRINT_DEADLOCK){
							if(newState.board.isDeadLock(getBoxDiff(this, newState))){
									System.out.println("=======DEADLOCK========");
									System.out.println(newState.toString(getBoxDiff(this, newState)));
									try {
						        		System.in.read();
									} catch (IOException e) {
										e.printStackTrace();
									}
							}
						}
						
						// If the board is solved we don't care if its a deadlock!
						if(newState.getBoard().isSolved()) {
							queue.add(newState);
						}
						
						if(safeSwap && !newState.board.isDeadLock(getBoxDiff(this, newState)) && !visitedBoards.contains(newState.hashCode())){
													//System.out.println("Added Left");
							visitedBoards.add(newState.hashCode());
							newState.board.safeswapCoordinates(nextCoordinate, topLeft);
							if(!newState.board.isDeadLock(getBoxDiff(this, newState))){
								queue.add(newState);
								newState.path.add('L');
	
							}

						}
					}
				}

				if(board.canMoveBoxRight(c)){

					State newState = new State();
					newState.setBoard(board.copy());
					newState.path = (LinkedList<Character>) this.path.clone();
					newState.path.addAll(newState.board.moveFromCoordinateToCoordinateUsingRoom(topLeft, c, room));
					newState.board.safeswapCoordinates(c, topLeft);
					newState.board = newState.board.goRight();
					if(newState.board != null){
						Coordinate nextCoordinate = new Coordinate(c, 0, 1);
	
						boolean safeSwap = newState.board.safeswapCoordinates(nextCoordinate, topLeft);
	
	
						// ======= DEADLOCK PRINT =========
						if(PRINT_DEADLOCK){
							if(newState.board.isDeadLock(getBoxDiff(this, newState))){
									System.out.println("=======DEADLOCK========");
									System.out.println(newState.toString(getBoxDiff(this, newState)));
									try {
						        		System.in.read();
									} catch (IOException e) {
										e.printStackTrace();
									}
							}
						}
						
						// If the board is solved we don't care if its a deadlock!
						if(newState.getBoard().isSolved()) {
							queue.add(newState);
						}
						
						if(safeSwap && !newState.board.isDeadLock(getBoxDiff(this, newState)) && !visitedBoards.contains(newState.hashCode())){
													//System.out.println("Added Right");
							visitedBoards.add(newState.hashCode());
							newState.board.safeswapCoordinates(nextCoordinate, topLeft);
							if(!newState.board.isDeadLock(getBoxDiff(this, newState))){
								queue.add(newState);
								newState.path.add('R');
	
							}

						}
					}
				}
			}
		}


		/*
		 * 1. find all visitable Coordinates in the same "room" (goals and spaces)
		 * 2. the top left corner in visitable Coordinates should represent the "current state" and be represented as the hashcode
		 * 3. for all Coordinates c in visitable Coordinates find the add the valid moves, that moves a box to the queue (if not previous state, deadlock etc).
		 * 
		 */


		/*	
		// UP
		//
		// Create a new state and set the board to some UP-move.
		//System.out.println("UP");
		State upState = new State();
		upState.setBoard(this.getBoard().goUp());
		upState.path = ((LinkedList<Character>)this.path.clone());
		upState.path.add(new Character('U'));

		// If the state was legal, that is, the board is not null and we havnt already had it in the
		// queue, insert it into the queue.
		if(upState.getBoard() != null && visitedBoards.indexOf(upState.hashCode()) == -1) {

			if (!upState.getBoard().isDeadLock(getBoxDiff(this, upState)))  {
				//System.out.println("\tAdded move!");
				visitedBoards.add(upState.hashCode());
				queue.add(upState);
			}
			else {
	        	try {
	        		//System.out.println("\tFound a deadlock state. Show? (y)");
	        		String in = scanner.nextLine();
	        		if (in.length() > 0) {
	        			//System.out.println("DEADLOCKSTATE");
	        			//System.out.println(upState.toString());
	        		}
				} catch (Exception e) {
					e.printStackTrace();
				}


		} else {
			if(upState.getBoard() == null) {
				//System.out.println("\tDiscard up move. Move was not legal.");
			}
			else if (visitedBoards.indexOf(upState.hashCode()) != -1) {
				//System.out.println("\tDiscard up move. Board has already been visited.");
			}
		}

		// DOWN
		//
		// Create a new state and set the board to some DOWN-move.
		//System.out.println("DOWN");
		State downState = new State();
		downState.setBoard(this.getBoard().goDown());
		downState.path = ((LinkedList<Character>)this.path.clone());
		downState.path.add(new Character('D'));

		// If the state was legal, that is, the board is not null and we havnt already had it in the
		// queue, insert it into the queue.
		if(downState.getBoard() != null && visitedBoards.indexOf(downState.hashCode()) == -1) {

			if (!downState.getBoard().isDeadLock(getBoxDiff(this, downState)))  {
				//System.out.println("\tAdded down move!");
				visitedBoards.add(downState.hashCode());
				queue.add(downState);
			}
			else {
	        	try {
	        		System.out.println("\tFound a deadlock state. Show? (y)");
	        		String in = scanner.nextLine();
	        		if (in.length() > 0) {
	        			System.out.println("DEADLOCKSTATE");
	        			System.out.println(downState.toString());
	        		}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} else {
			if(downState.getBoard() == null) {
				//System.out.println("\tDiscard down move. Move was not legal.");
			}
			else if (visitedBoards.indexOf(downState.hashCode()) != -1) {
				//System.out.println("\tDiscard down move. Board has already been visited.");
			}
		}

		// LEFT
		//
		// Create a new state and set the board to some DOWN-move.
		//System.out.println("LEFT");
		State leftState = new State();
		leftState.setBoard(this.getBoard().goLeft());
		leftState.path = ((LinkedList<Character>)this.path.clone());
		leftState.path.add(new Character('L'));

		// If the state was legal, that is, the board is not null and we havnt already had it in the
		// queue, insert it into the queue.
		if(leftState.getBoard() != null && visitedBoards.indexOf(leftState.hashCode()) == -1) {

			if (!leftState.getBoard().isDeadLock(getBoxDiff(this, leftState)))  {
				//System.out.println("\tAdded left move!");
				visitedBoards.add(leftState.hashCode());
				queue.add(leftState);
			} 
			else {
	        	try {
	        		//System.out.println("\tFound a deadlock state. Show? (y)");
	        		String in = scanner.nextLine();
	        		if (in.length() > 0) {
	        			//System.out.println("\tDEADLOCKSTATE");
	        			//System.out.println(leftState.toString());
	        		}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} else {
			if(leftState.getBoard() == null) {
				//System.out.println("\tDiscard left move. Move was not legal.");
			}
			else if (visitedBoards.indexOf(leftState.hashCode()) != -1) {
				//System.out.println("\tDiscard left move. Board has already been visited.");
			}
		}

		// RIGHT 
		//
		// Create a new state and set the board to some DOWN-move.
		//System.out.println("RIGHT");
		State rightState = new State();
		rightState.setBoard(this.getBoard().goRight());
		rightState.path = ((LinkedList<Character>)this.path.clone());
		rightState.path.add(new Character('R'));

		// If the state was legal, that is, the board is not null and we havnt already had it in the
		// queue, insert it into the queue.
		if(rightState.getBoard() != null && visitedBoards.indexOf(rightState.hashCode()) == -1) {


			if (!rightState.getBoard().isDeadLock(getBoxDiff(this, rightState)))  {
				//System.out.println("\tAdded right move!");
				visitedBoards.add(rightState.hashCode());
				queue.add(rightState);
			} /*
			else {
	        	try {
	        		//System.out.println("\tFound a deadlock state. Show? (y)");
	        		String in = scanner.nextLine();
	        		if (in.length() > 0) {
	        			//System.out.println("\tDEADLOCKSTATE");
	        			//System.out.println(rightState.toString());
	        		}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} else {
			if(rightState.getBoard() == null) {
				//System.out.println("\tDiscard right move. Move was not legal.");
			}
			else if (visitedBoards.indexOf(rightState.hashCode()) != -1) {
				//System.out.println("\tDiscard right move. Board has already been visited.");
			}
		}

		 */
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	/**
	 * The hashed value of a state is the hashed value of its board.
	 * @return hash of the states board.
	 */
	public int hashCode() {
		if( this.getBoard() == null) {
			return -1;
		}
		return this.board.hashCode();
	}

	public int hashCodeTwo(char[][] board){
		for(int i = 0; i < board.length; i++){
			for(int j = 0; j < board[i].length; j++){

			}
		}
		return -1;
	}

	@Override
	public int compareTo(State otherState) {
		int otherGoals = Board.getAllBoxesOnGoals(otherState.getBoard()).size();
		int myGoals = Board.getAllBoxesOnGoals(otherState.getBoard()).size();
		
		if (otherGoals != myGoals) {
			return Board.getAllBoxesOnGoals(otherState.getBoard()).size() - Board.getAllBoxesOnGoals(board).size();
		}

		int myPenalty = 0;
		int otherPenalty = 0;
		
		myPenalty = this.getBoard().puzzleHeuristic();
		otherPenalty = otherState.getBoard().puzzleHeuristic();
	
		/*
		System.out.println("Equal amount of goals. Checking box distance");
		System.out.println(this.getBoard().toString());
		System.out.println("myPenalty: " + myPenalty);
		System.out.println(otherState.toString());
		System.out.println("otherPenalty: " + otherPenalty);
		
		// Halt for input.
    	try {
    		System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		*/
	
		if (myPenalty <= otherPenalty)
			return -1;
		
		return 1;
	}

	/**
	 * Returns a coordinate to a moved box.
	 */
	static public Coordinate getBoxDiff(State two, State one) {
		final boolean debug = false;
		
		LinkedList<Coordinate> pos1 = (LinkedList<Coordinate>) Board.getAllBoxes(one.getBoard());
		LinkedList<Coordinate> pos2 = (LinkedList<Coordinate>) Board.getAllBoxes(two.getBoard());
		Coordinate p1, p2 = null;

		for(int i = 0; i < pos2.size(); i++){
			p2 = pos2.get(i);
			for(int j = 0; j < pos1.size(); j++){
				p1 = pos1.get(j);
				if(p1.getColumn() == p2.getColumn() && p1.getRow() == p2.getRow()){
					pos1.remove(j);
				}
			}
		}
		
		if(debug) {
			System.out.println("getBoxDiff(state1, state2)");
			System.out.println("--------------------------");
			System.out.println(two.toString());
			System.out.println(one.toString());
			System.out.println("--------------------------");
			System.out.println("Diffs: " + pos1.size());
			if(pos1.size() > 0)
				System.out.println("Diff: " + pos1.get(0).toString());
			System.out.println("--------------------------");
		}

		if(pos1.size() == 1)
			return pos1.getFirst();
		
		return null;
	}

	/**
	 * Gets the players position.
	 * @return position of the player
	 */
	public Coordinate getPos(char[][] board) {
		return Board.getPlayerPosition(this.getBoard());
	}

	public void setPos(Coordinate pos) {
		this.pos = pos;
	}

}
